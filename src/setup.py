from setuptools import setup, find_packages

setup(
    name='moodle-questions',  # This is the name of your PyPI-package.
    version='0.0.1',  # Update the version number for new releases
    packages=find_packages(),
    zip_safe=False
)
