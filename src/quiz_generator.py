#!/usr/bin/env python3

from tkinter import Tk
from gui.gui import GeneratorGUI

# Initializa main window and pass it to the GeneratorGUI class for further processing
def mainFunction(dim, title):
  '''
  A simple function which generates a new GUI frame with set parameters starts the GeneratorGUI within the frame.

  :param dim: a string with GUI window dimmensions.
  :param title: a string with GUI window title.
  '''
  root = Tk()
  root.title(title)
  root.geometry( dim )
  app = GeneratorGUI(root)
  root.mainloop()
  
if __name__ == "__main__":
  const_geo = "600x400"
  const_ttl = "moodle-uml-quiz-generator"

  mainFunction(const_geo, const_ttl)