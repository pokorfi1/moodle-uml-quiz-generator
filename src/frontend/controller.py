from PIL import Image
from pathlib import Path
from os import remove

from src.moodle_questions import DragImage, DragText
from src.moodle_questions import DropZone
from src.moodle_questions import DragAndDropOntoImageQuestion, DragAndDropIntoTextQuestion
from src.moodle_questions import Quiz

class Cropper(object):


  def slice_file(self, sname, coords):
    '''
    Crops input image and saves as a new file.

    :param coords: a touple of coordinates for the cropping (x0, y0, x1, y1)
    '''
    f = self._img.crop(coords)
    f.save(sname)

  def slice_cycle(self):
    '''
    Main Export function. Interates through drawn objects in the window and exports them to the Quiz

    '''
    quiz = Quiz()
    tmpFiles = []

    #Taken from DragAndDrop example
    #TODO Add variable question initialization to the GUI
    question = DragAndDropOntoImageQuestion(name="Otazka cislo 1",
                                      background=self._filename,
                                      question_text="Priradte obrazky do chlivecku.",
                                      default_mark=1,
                                      correct_feedback="Gratulace!",
                                      shuffle=True)

    fname = Path(self._filename)
    for coords in self._coordsArray:
      sname = "./tmp"+str(self._coordsArray.index(coords))+".jpeg"
      self.slice_file(sname, coords)
      question.add_dragitem(DragImage(sname, unlimited=True), [DropZone(coords[0], coords[3])])
      quiz.add_question(question)
      tmpFiles.append(sname)
    
    quiz.save(str(fname.with_suffix('.xml')))

    for f in tmpFiles:
      remove(f)


    return str(fname.with_suffix('.xml'))


  def __init__(self, fname, tpl, log):
    '''
    Start-Up function for the class.

    :param tuple: an array of coordinate touples 
    :parma fname: filename of the image to be processed.
    '''
    self._logger = log
    self._coordsArray = tpl
    self._filename = fname
    self._img = Image.open(self._filename, 'r')


