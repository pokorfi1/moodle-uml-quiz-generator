from tkinter import Frame, Tk, messagebox, BOTH, Menu, filedialog, Label, Canvas

from PIL import ImageTk, Image

from src.frontend.controller import Cropper

from logging import getLogger, StreamHandler, Formatter, DEBUG, debug, INFO, info, WARNING, warning, ERROR, error, CRITICAL, critical

class GeneratorGUI(Frame):
  '''
  Class generates the canvas within the provided GUI window Frame.
  The Class handless GUI related functionality so the Frame and references does not have to be passed between classes.

  :param Frame: a Frame object for the GUI
  '''

  #START Menu Item functions

  #TODO: Add Undo, Delete Object
  def todo(self):
    '''
    Dummy TO-DO Function
    '''
    self.logger.debug("TODO Function!")
    return

  def selectDropZone(self):
    '''
    A simple switch function which defines what the current mode of object selection is used.
    The value is stored on the class level. The value is then passed as a tag to the given drawn object and used in the export function.
    #TODO rework the logic a bit so it does not have to relly on string switch.
    '''
    self.current_tag = "dropZone"

  def export(self):
    export_touple = []
    for elem in self.canvas.find_all()[1:]:
      coords = self.canvas.coords(elem)
      self.logger.debug("Exporting item at: "+str(coords[0])+"x"+str(coords[1]))
      if len(coords) == 4:
        continue
      bbox = (coords[0]-75, coords[1]-50, coords[0]+75, coords[1]+50)
      export_touple.append(bbox)
    cropper = Cropper(self.filename, export_touple, self.logger)
    outfile = cropper.slice_cycle()
    self.logger.info("Diagram exported to XML file: "+outfile)
    messagebox.showinfo("Diagram exported", "Diagram exported to XML file: "+outfile)


  def help(self):
    '''
    Simple help poping window
    '''
    messagebox.showinfo("Help", "On loaded image, to select area for the draggable question, point on into the center of the question and right-click. To deselect left-click into the selected area.")

  def exitWindow(self):
    '''
    Closes the window without sanizatitaion.
    #TODO Add closing dialog, save options etc.
    '''
    exit()

  def startDebug(self):
    '''
    Changes logger debug level
    '''
    self.logger.setLevel(DEBUG)
    return

  def stopDebug(self):
    '''
    Changes logger debug level
    '''
    self.logger.setLevel(INFO)
    return


  def onOpen(self):
    '''
    The function loads a new image, updates the frame size to match and resets the rectid counter.
    '''

    ftypes = [('JPEG files', '*.jpeg'), ('All files', '*')]
    dlg = filedialog.Open(self, filetypes = ftypes)
    fl = dlg.show()

    if not fl:
      self.logger.debug("File not loaded!")
      return
        
    img_to_open = Image.open(fl)
    new_h = img_to_open.height
    new_w = img_to_open.width
    self.logger.debug("New windows size is: "+str(new_h)+"x"+str(new_w))


    self.filename = fl
    self.img = ImageTk.PhotoImage(img_to_open)

    if self.image is not None:
      self.canvas.delete(self.img)

    self.canvas = Canvas(self.parent, width = new_w, height = new_h, bg = "white" )
    self.canvas.create_image((new_w / 2, new_h / 2), image=self.img)

    self.canvas.grid(row=0, column=0, sticky='nsew')
    self._createCanvasBinding()

    self.rectid = 0

    self.parent.geometry(str(new_w)+"x"+str(new_h))

  #END Menu Item functions

  #START Initialization Functions
  def _initUI(self): #Defines the Menu Bar items
    '''
    The function intitializes the top bar.
    '''
    menubar = Menu(self.parent)
    self.parent.config(menu=menubar)
    fileMenu = Menu(menubar)
    fileMenu.add_command(label="Open", command=self.onOpen)
    fileMenu.add_command(label="Export", command=self.export)
    fileMenu.add_command(label="Exit", command=self.exitWindow)
    menubar.add_cascade(label="File", menu=fileMenu)

    editMenu = Menu(menubar)
    editMenu.add_command(label="Select Dropzone", command=self.selectDropZone)
    editMenu.add_command(label="Select Text Fill", command=self.todo)
    menubar.add_cascade(label="Edit", menu=editMenu)

    debugMenu = Menu(menubar)
    debugMenu.add_command(label="Start Debug", command=self.startDebug)
    debugMenu.add_command(label="Stop Debug", command=self.stopDebug)
    menubar.add_cascade(label="Debug", menu=debugMenu)

    helpMenu = Menu(menubar)
    helpMenu.add_command(label="Help", command=self.help)
    menubar.add_cascade(label="Help", menu=helpMenu)


  def _initLogger(self):
    '''
    The function starts the logging facility.
    '''
    # Setup Logging facility
    self.logger = getLogger("Moodle-UML-Quiz-Generator")
    self.logger.setLevel(INFO)
    self.ch = StreamHandler()
    self.ch.setLevel(DEBUG)
    self.formatter = Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    self.ch.setFormatter(self.formatter)
    self.logger.addHandler(self.ch)

  def _createVariables(self, parent): #Defines class variables
    '''
    The function defines used object variables.

    :object param current_tag: is used as a tag to drawn objects, based on the tag a different set of exports can be made.
    :object param filename: filename of the currently opened image
    :object param image: reference to the current object
    :object param parent: GUI parent window
    :object param rectid: counter of drawn objects on the current image
    '''
    self.current_tag = "dropZone"
    # used to indicate current selection mode, can be either "dropZone" or "fillText"
    self.filename = None
    # filename of the currently loaded file, filename is used for conseqent objects exported
    self.image = None
    # reference to the currenty edited image
    self.parent = parent
    # parent window used for canvas drawing
    self.rectid = 0
    # id of currently drawn object, is reset once a new image is loaded

  def _createCanvas(self): #Creates Canvas object for drawing with default size and bg
    '''
    Initializes the canvas within the GUI frame
    '''
    self.canvas = Canvas(self.parent, width = 640, height = 480, bg = "white" )
    self.canvas.grid(row=0, column=0, sticky='nsew')
    #TODO: start window on current screen

  def _createCanvasBinding(self): #Binds Mouse actions to the window canvas
    '''
    Bind mouse buttons to the current Canvas.
    Button-1: is used for adding new drawn object
    Button-2: is used for deleting drawn object
    '''
    self.canvas.bind( "<Button-1>", self.addDropZone )
    self.canvas.bind( "<Button-2>", self.delDropZone )

  #END Initialization Functions

  #START On mouse event functions, functions are used for drawing rectangles
  def addDropZone(self, event):
    '''
    Adds a dropZone object to the current canvas (on top of the image).
    The function takes the mouse click coordiantes as a center for the drawn object which has preset size.
    The size is 150x100 as per default moodle configuration.
    Two objects are created, a text box with object number (rectid) and a background object. Only text box object is exported.

    :param event: is a mouse-event object which click coordinates.
    '''
    if (len(self.canvas.find_all()) == 0):
      return
    tx = self.canvas.create_text((event.x, event.y), text=" Drop Zone: "+str(self.rectid)+str(" "))
    bg = self.canvas.create_rectangle((event.x-75, event.y-50, event.x+75, event.y+50),fill="#4eccde")
    self.canvas.tag_lower(bg, tx) #create_text does not support background, so a second box has to be created
    self.rectid += 1
    self.logger.debug("Adding DropZone near: "+str(event.x)+" x "+str(event.y))

  def delDropZone(self, event):
    '''
    Deletes a dropZone near the click area. The function iterates through all drawn objects and if the coordinates are within the box, the object is deleted.
    Please see addDropZone comments for dropZone details.

    :param event: is a mouse-event object which click coordinates.
    '''
    if (len(self.canvas.find_all()) == 0):
      return
    for elem in self.canvas.find_all()[1:]:
      coords = self.canvas.coords(elem)
      x, y = 0, 0
      if len(coords) == 4: #for background box, take the center of the box
        x = (coords[0]+coords[2])/2
        y = (coords[1]+coords[3])/2
      elif len(coords) == 2:
        x = coords[0]
        y = coords[1]
      if (event.x < x+75 and event.x > x-75 and event.y < y+20 and event.y > y-20): 
        self.canvas.delete(elem) #delete item if we are box-size (150x100) around the item
    self.logger.debug("Deleting Item near: "+str(event.x)+" x "+str(event.y) )
    #TODO Fix delete underbox (DONE)

  #END On mouse event functions

  def __init__(self, parent):
    '''
    Start-Up function for the class.

    parent: parent GUI window.
    '''
    Frame.__init__(self, parent)
    self._initLogger()
    self._createVariables(parent)
    self._createCanvas()
    self._createCanvasBinding()

    self._initUI()
