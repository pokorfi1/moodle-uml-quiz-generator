#!/usr/bin/env python3

from tkinter import Tk
from src.frontend.ui import GeneratorGUI

# Initializa main window and pass it to the GeneratorGUI class for further processing
def main():
  '''
  A simple function which generates a new GUI frame with set parameters starts the GeneratorGUI within the frame.

  :param dim: a string with GUI window dimmensions.
  :param title: a string with GUI window title.
  '''
  const_geo = "600x400"
  const_ttl = "moodle-uml-quiz-generator"

  root = Tk()
  root.title( const_ttl )
  root.geometry( const_geo )
  app = GeneratorGUI(root)
  root.mainloop()
  
if __name__ == "__main__":
  main()