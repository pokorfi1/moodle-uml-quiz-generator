frontend package
================

frontend.ui module
------------------

.. automodule:: frontend.ui
   :members:
   :undoc-members:
   :show-inheritance:


frontend.controller module
--------------------------

.. automodule:: frontend.controller
   :members:
   :undoc-members:
   :show-inheritance:
