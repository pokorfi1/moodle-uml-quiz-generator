Example usage of moodle-questions package
=========================================

.. code-block:: python

    from moodle_questions import Quiz, DragAndDropOntoImageQuestion, DragImage, DragText, DropZone

    quiz = Quiz()

    question = DragAndDropOntoImageQuestion(name="Otazka cislo 1",
                                            background="/tmp/background.png",
                                            question_text="Priradte obrazky do chlivecku.",
                                            default_mark=1,
                                            correct_feedback="Gratulace!",
                                            shuffle=True)

    question.add_dragitem(DragImage("/tmp/item1.jpg", unlimited=True), [DropZone(100, 100)])

    question.add_dragitem(DragText("Just a text."), [DropZone(300,100)])

    quiz.add_question(question)

    quiz.save("test.xml")
