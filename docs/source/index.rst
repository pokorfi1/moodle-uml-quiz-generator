.. moodle-questions documentation master file, created by
   sphinx-quickstart on Sun Jun 23 15:29:38 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Moodle UML Quiz Generator's documentation!
=====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   example.rst
   schema.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
