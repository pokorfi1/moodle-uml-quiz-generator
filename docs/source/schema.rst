Moodle XMLSchema of DragAndDropOntoImageQuestion
================================================

.. code-block:: xml

    <?xml version="1.0" encoding="utf-8" ?>
    <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
        <xs:element name="question">
            <xs:complexType>
                <xs:sequence>
                    <xs:element name="name">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element type="xs:string" name="text"/>
                            </xs:sequence>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="questiontext">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element type="xs:string" name="text"/>
                            </xs:sequence>
                            <xs:attribute type="xs:string" name="format"/>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="generalfeedback">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element type="xs:string" name="text"/>
                            </xs:sequence>
                            <xs:attribute type="xs:string" name="format"/>
                        </xs:complexType>
                    </xs:element>
                    <xs:element type="xs:float" name="defaultgrade"/>
                    <xs:element type="xs:float" name="penalty"/>
                    <xs:element type="xs:byte" name="hidden"/>
                    <xs:element type="xs:string" name="idnumber"/>
                    <xs:element type="xs:string" name="shuffleanswers"/>
                    <xs:element name="correctfeedback">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element type="xs:string" name="text"/>
                            </xs:sequence>
                            <xs:attribute type="xs:string" name="format"/>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="partiallycorrectfeedback">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element type="xs:string" name="text"/>
                            </xs:sequence>
                            <xs:attribute type="xs:string" name="format"/>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="incorrectfeedback">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element type="xs:string" name="text"/>
                            </xs:sequence>
                            <xs:attribute type="xs:string" name="format"/>
                        </xs:complexType>
                    </xs:element>
                    <xs:element type="xs:string" name="shownumcorrect"/>
                    <xs:element name="file">
                        <xs:complexType>
                            <xs:simpleContent>
                                <xs:extension base="xs:string">
                                    <xs:attribute type="xs:string" name="name"/>
                                    <xs:attribute type="xs:string" name="encoding"/>
                                </xs:extension>
                            </xs:simpleContent>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="drag" maxOccurs="unbounded" minOccurs="0">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element type="xs:integer" name="no"/>
                                <xs:element type="xs:string" name="text"/>
                                <xs:element type="xs:integer" name="draggroup"/>
                                <xs:element type="xs:string" name="infinite" minOccurs="0"/>
                                <xs:element name="file">
                                    <xs:complexType>
                                        <xs:simpleContent>
                                            <xs:extension base="xs:string">
                                                <xs:attribute type="xs:string" name="name" use="required"/>
                                                <xs:attribute type="xs:string" name="encoding" fixed="base64"/>
                                            </xs:extension>
                                        </xs:simpleContent>
                                    </xs:complexType>
                                </xs:element>
                            </xs:sequence>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="drop" maxOccurs="unbounded" minOccurs="0">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element type="xs:string" name="text"/>
                                <xs:element type="xs:byte" name="no"/>
                                <xs:element type="xs:byte" name="choice"/>
                                <xs:element type="xs:short" name="xleft"/>
                                <xs:element type="xs:short" name="ytop"/>
                            </xs:sequence>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="hint" maxOccurs="unbounded" minOccurs="0">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element type="xs:string" name="text"/>
                            </xs:sequence>
                            <xs:attribute type="xs:string" name="format" use="optional"/>
                        </xs:complexType>
                    </xs:element>
                    <xs:element name="tags">
                        <xs:complexType>
                            <xs:sequence>
                                <xs:element name="tag">
                                    <xs:complexType>
                                        <xs:sequence>
                                            <xs:element type="xs:string" name="text"/>
                                        </xs:sequence>
                                    </xs:complexType>
                                </xs:element>
                            </xs:sequence>
                        </xs:complexType>
                    </xs:element>
                </xs:sequence>
                <xs:attribute name="type" type="xs:string" fixed="ddimageortext"/>
            </xs:complexType>
        </xs:element>
    </xs:schema>