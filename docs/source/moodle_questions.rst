moodle\_questions package
=========================

Submodules
----------

moodle\_questions.dragitem module
---------------------------------

.. automodule:: moodle_questions.dragitem
   :members:
   :undoc-members:
   :show-inheritance:


moodle\_questions.dropzone module
---------------------------------

.. automodule:: moodle_questions.dropzone
   :members:
   :undoc-members:
   :show-inheritance:

moodle\_questions.question module
---------------------------------

.. automodule:: moodle_questions.question
   :members:
   :undoc-members:
   :show-inheritance:

moodle\_questions.quiz module
-----------------------------

.. automodule:: moodle_questions.quiz
   :members:
   :undoc-members:
   :show-inheritance:
