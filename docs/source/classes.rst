..
   Quiz
   ----

   .. autoclass:: moodle_questions.Quiz
      :members:
      :undoc-members:

   DragAndDropOntoImageQuestion
   ----------------------------

   .. autoclass:: moodle_questions.DragAndDropOntoImageQuestion
      :members:
      :undoc-members:

   DragAndDropIntoTextQuestion
   ---------------------------

   .. autoclass:: moodle_questions.DragAndDropIntoTextQuestion
      :members:
      :undoc-members:

   DragText
   --------

   .. autoclass:: moodle_questions.DragText
      :members:
      :undoc-members:


   DragImage
   ---------

   .. autoclass:: moodle_questions.DragImage
      :members:
      :undoc-members:


   DropZone
   --------

   .. autoclass:: moodle_questions.DropZone
      :members:
      :undoc-members:
