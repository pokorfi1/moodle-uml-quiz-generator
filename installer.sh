#!/bin/bash

VERSION=$(python -V 2>&1 | cut -d\  -f 2) # python 2 prints version to stderr
VERSION=(${VERSION//./ }) # make an version parts array
USE_PY=""
if [[ ${VERSION[0]} -lt 3 ]] || [[ ${VERSION[0]} -eq 3 && ${VERSION[1]} -lt 4 ]] ; then
  VERSION2=$(python3 -V | cut -d\  -f 2)
  VERSION2=(${VERSION2//./ })
  if [[ ${VERSION2[0]} -lt 3 ]] || [[ ${VERSION2[0]} -eq 3 && ${VERSION2[1]} -lt 4 ]] ; then
    echo "[-] Python 3.4+ needed! Aborting" 1>&2
    return 1
  else
    USE_PY="3"
  fi
fi
echo "[+] Correct Python3 version installed ... continuing"

PIP_CMD="pip"${USE_PY}
if hash ${PIP_CMD} 2>/dev/null ; then
  echo "[+] Pip command found ... continuing"
else
  echo "[-] Pip for Python3 needed! Aborting" 1>&2
fi

#TODO Add virtual Env creator

${PIP_CMD} install -r ./requirements.txt

LINK="/usr/local/bin/umlQuizGen"
NAME="$(pwd)/run.sh"
echo "[+] Creating Launcher"
echo "python${USE_PY} $(pwd)/main.py" > ${NAME}
chmod +x ${NAME}
rm -f ${LINK} 2>/dev/null
ln -s ${NAME} ${LINK}
